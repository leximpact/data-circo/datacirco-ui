# LexImpact DataCirco UI

- [Toufic Batache](mailto:toufic.batache@assemblee-nationale.fr)
- Nehmat Hatoum
- [Dorine Lambinet](mailto:dorine.lambinet@assemblee-nationale.fr)
- [Emmanuel Raviart](mailto:emmanuel.raviart@assemblee-nationale.fr)

Copyright (C) 2023 Assemblée nationale

https://git.leximpact.dev/leximpact/barometre-application-lois/echeancier/

> LexImpact DataCirco UI is free software; you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, either version 3 of the
> License, or (at your option) any later version.
>
> LexImpact DataCirco UI is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program. If not, see <http://www.gnu.org/licenses/>.
