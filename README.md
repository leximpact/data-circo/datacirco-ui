# DataCirco : interface Web et export PDF

Prototype de version Web disponible sur https://datacirco-dev.leximpact.dev/circonscriptions/091-07

## Installation

L'utilisation de NVM permet de gérer les versions de Node et de s'affranchir des limitations de version des distributions.

```sh
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
source ~/.bashrc
nvm install node
npm install
npx playwright install
```

## Developing

Start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

Go to http://127.0.0.1:5173/circonscriptions/091-07

## Génération d'un PDF

http://localhost:5173/circonscriptions/091-07/impression

`wget -O 971-01.pdf http://localhost:5173/circonscriptions/091-07/impression`

`export CIRCO=091-07; wget -O $CIRCO.pdf "http://localhost:5173/circonscriptions/$CIRCO/impression"`

L'erreur _"waiting for locator('#precipitations').or(locator('#dpe')).first() to be visible"_ signifie que la circo n'existe probablement pas.

## Génération de tous les PDF

Il faut avoir installer :

- ImageMagick pour créer la vignette
- Ghostscript pour la reduction de taille du PDF
- optipng pour la réduction de taille du PNG
- qrcode pour la génération du QR code.

```sh
apt update && apt upgrade -y
apt -y install wget nano git curl optipng imagemagick-6.q16hdri ghostscript
pip install qrcode[pil]
```

```sh
generate_pdf.sh
```

Il faut compter 1h30.

Pour le détail de la production LexImpact, voir [La doc DevOps](https://git.leximpact.dev/leximpact/communs/devops/-/blob/master/notes_installations/datacirco.md) (accès restreint).

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.
