import {
    type DataEducation,
    type EducationInfoPS,
  } from "$lib/education_data"

interface ColorMap {
    [key: string]: string;
}

// Correspondance entre les couleurs et les valeurs HTML
const color_map: ColorMap = {
    cyan: "#00E0D7",
    blue: "#1132FC",
    violet: "#7800F1",
    fuchsia: "#D40EC3",
    red: "#C50140",
    orange: "#FF553E",
    goldenrod: "#FFAB01",
    grey: "#686868",
};

export function getColorFromIps(ips: number | undefined): string {
    if (ips === undefined) {
        return color_map["grey"];
    }
    ips = Number(ips || 0);
    
    let color: string;
    if (ips < 25) {
        color = "grey";
    } else if (ips < 90) {
        color = "goldenrod";
    } else if (ips < 100) {
        color = "orange";
    } else if (ips < 110) {
        color = "red";
    } else if (ips < 120) {
        color = "fuchsia";
    } else if (ips < 130) {
        color = "violet";
    } else if (ips < 140) {
        color = "blue";
    } else {
        color = "cyan";
    }
    return color_map[color];
}

export function getColorFromEffectif(effectifs: number | null): string {
    if (effectifs === null) {
        return color_map["grey"];
    }
    effectifs = Number(effectifs || 0);
    let color: string;
    if (effectifs < 1) {
        color = "grey";
    } else if (effectifs < 100) {
        color = "goldenrod";
    } else if (effectifs < 200) {
        color = "orange";
    } else if (effectifs < 300) {
        color = "red";
    } else if (effectifs < 500) {
        color = "fuchsia";
    } else if (effectifs < 800) {
        color = "violet";
    } else if (effectifs < 1000) {
        color = "blue";
    } else {
        color = "cyan";
    }
    return color_map[color];
}
export function plot_map(
    div_name: string,
    customdata: EducationInfoPS[],
    hovertemplate,
    color,
    Plotly,
  ) {
    const map_etablissements = [
      {
        type: "scattermapbox",
        lat: customdata.map((d) => d.latitude),
        lon: customdata.map((d) => d.longitude),
        customdata: customdata,
        hovertemplate: hovertemplate,
        mode: "markers",
        marker: {
          size: 10,
          color: color,
          //symbol: 'triangle',
          //symbol: "triangle-up",
        },
      },
    ]

    const layout = {
      //showlegend: true,
      autosize: true,
      responsive: true,
      // width: 1000,
      // height: 800,
      paper_bgcolor: "rgba(0,0,0,0)",
      margin: { r: 0, t: 0, b: 0, l: 0, autoexpand: true },
      // title: "Etablissements scolaire de la circonscription",
      mapbox: {
        style: "open-street-map", // No need for a token
        // style: "basic", // Need a free mapbox token
        center: {
          lat: customdata[0].latitude,
          lon: customdata[0]
            .longitude,
        },
        zoom: 10,
      },
    }
    // if ($Plotly !== undefined) {
    Plotly.newPlot(
      div_name,
      map_etablissements,
      layout,
      //{ mapboxAccessToken: mapboxAccessToken}
    )
    // }
  }


  export function plot_ips(dataEducation: DataEducation, Plotly) {
    /*Carte de l'IPS des écoles primaires et secondaires*/

    const hovertemplate =
      "%{customdata.appellation_officielle}<br>Statut : %{customdata.secteur_public_prive_libe}<br>%{customdata.nature_uai_libe}<br>Indices de position sociale : %{customdata.ips}<extra></extra>"
    const color = dataEducation.etablissements_premier_et_second_degre.map(
      (d) => getColorFromIps(d.ips) )

    plot_map(
      "plot_ips",
      dataEducation.etablissements_premier_et_second_degre,
      hovertemplate,
      color,
      Plotly
    )
  }

  export function plot_effectifs(dataEducation: DataEducation, Plotly) {
    /*Carte de l'effectif des écoles primaires, secondaires et supérieures */
    const hovertemplate =
      "%{customdata.appellation_officielle}<br>Statut : %{customdata.secteur_public_prive_libe}<br>%{customdata.nature_uai_libe}<br>Effectif : %{customdata.effectifs}<extra></extra>"
    const color = dataEducation.etablissements_tous.map(
      (d) =>  getColorFromEffectif(d.effectifs) )

    plot_map(
      "plot_effectifs",
      dataEducation.etablissements_tous,
      hovertemplate,
      color,Plotly
    )
  }


  export function plot_ips_histo(div_name: string, customdata: DataEducation, Plotly) {
    const data = [
      {
        type: "histogram",
        y: customdata.ips_premier_et_second_public_circo_list,
        name: "Public",
        opacity: 0.5,
        marker: {
          color: "#635f06",
        },
      },
      {
        type: "histogram",
        histfunc: "sum",
        orientation: "h",
        marker: {
          color: "#2F406A",
        },
        opacity: 0.5,
        x: Array(customdata.ips_premier_et_second_prive_circo_list.length).fill(
          -1,
        ),
        yaxis_autorange: "reversed",
        y: customdata.ips_premier_et_second_prive_circo_list,

        name: "Privé",
      },
    ]
    const ips_premier_et_second_prive_france = [
      customdata.ips_premier_et_second_prive_france_max,
      customdata.ips_premier_et_second_prive_france_min,
      customdata.ips_premier_et_second_prive_france,
    ]
    const ips_premier_et_second_france_labels = [
      "France IPS max",
      "France IPS min",
      "France IPS moyen",
    ]
    const shapes_ips_prive_france = ips_premier_et_second_prive_france.map(
      (ips) => ({
        type: "line",
        x0: 0,
        y0: ips,
        x1: -15,
        y1: ips,
        line: {
          color: "gray",
          width: 1,
          dash: "dot",
        },
      }),
    )
    const annotations_prive_france = ips_premier_et_second_prive_france.map(
      (ips) => ({
        x: -18,
        y: ips,
        xref: "x",
        yref: "y",
        ax: 0,
        ay: 0,
        text: ips_premier_et_second_france_labels[
          ips_premier_et_second_prive_france.indexOf(ips)
        ],
        font: {
          size: 10,
          color: "gray",
        },
        align: "left",
      }),
    )

    const ips_premier_et_second_public_france = [
      customdata.ips_premier_et_second_public_france_max,
      customdata.ips_premier_et_second_public_france_min,
      customdata.ips_premier_et_second_public_france,
    ]

    const shapes_ips_public_france = ips_premier_et_second_public_france.map(
      (ips) => ({
        type: "line",
        x0: 15,
        y0: ips,
        x1: 0,
        y1: ips,
        line: {
          color: "gray",
          width: 1,
          dash: "dot",
        },
      }),
    )
    const annotations_public_france = ips_premier_et_second_public_france.map(
      (ips) => ({
        x: 18,
        y: ips,
        xref: "x",
        yref: "y",
        ax: 0,
        ay: 0,
        text: ips_premier_et_second_france_labels[
          ips_premier_et_second_public_france.indexOf(ips)
        ],
        font: {
          size: 10,
          color: "gray",
        },
      }),
    )
    const shape_ips_public_circo = {
      type: "line",
      x0: 10,
      y0: customdata.ips_premier_et_second_public_circo,
      x1: 0,
      y1: customdata.ips_premier_et_second_public_circo,
      line: {
        color: "#424004",
        width: 2,
        dash: "dot",
      },
    }
    const shape_ips_prive_circo = {
      type: "line",
      x0: 0,
      y0: customdata.ips_premier_et_second_prive_circo,
      x1: -10,
      y1: customdata.ips_premier_et_second_prive_circo,
      line: {
        color: "#2F406A",
        width: 2,
        dash: "dot",
      },
    }
    const annotations_prive_circo = {
      x: -13,
      y: customdata.ips_premier_et_second_prive_circo,
      xref: "x",
      yref: "y",
      ax: 0,
      ay: 0,
      text: "IPS moyen",
      font: {
        size: 10,
        color: "#2F406A",
      },
      align: "left",
    }

    const annotations_public_circo = {
      x: 13,
      y: customdata.ips_premier_et_second_public_circo,
      xref: "x",
      yref: "y",
      ax: 0,
      ay: 0,
      text: "IPS moyen",
      font: {
        size: 10,
        color: "#424004",
        textalign: "left",
      },
      position: "left",
    }
    const layout = {
      //title: "Répartition des établissements par IPS",
      width: 800,
      height: 500,
      margin: {
        t: 30, 
      },
      paper_bgcolor: "rgba(0,0,0,0)",
      barmode: "overlay",
      bargap: 0.0,
      bargroupgap: 0.0,
      xaxis: {
        title: "Nombre d'établissements",
        tickvals: [-20, -15, -10, -5, 0, 5, 10, 15, 20],
        ticktext: [20, 15, 10, 5, 0, 5, 10, 15, 20],
      },
      yaxis: {
        title: "IPS",
        zeroline: false,
      },
      shapes: [
        ...shapes_ips_public_france,
        shape_ips_public_circo,
        shape_ips_prive_circo,
        ...shapes_ips_prive_france,
      ],
      annotations: [
        ...annotations_prive_france,
        annotations_public_circo,
        annotations_prive_circo,
        ...annotations_public_france,
      ],
    }

    Plotly.newPlot(div_name, data, layout)
  }
