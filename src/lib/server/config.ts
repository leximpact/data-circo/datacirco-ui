import "dotenv/config"

export interface Config {
  root_url: string
}

const config: Config = { root_url: process.env.PUBLIC_DATACIRCO_ROOT_URL as string }
export default config
