// import Education from "./components/Education.svelte"

export interface EducationInfoPS {
    latitude: number
    longitude: number
    appellation_officielle?: string
    secteur_public_prive_libe: string
    nature_uai_libe: string // Unique to primary and secondary schools
    ips: number // Unique to primary and secondary schools
    effectifs: number
    taux_de_reussite: number // Unique to primary and secondary schools
    valeur_ajoutee: number // Unique to primary and secondary schools
    libelle_commune: string
    etab_prioritaire?: string
  }
  export interface EducationInfoSup {
    latitude: number
    longitude: number
    appellation_officielle?: string
    libelle: string // Pour les établissements supérieurs
    type_d_etablissement: string // Pour les établissements supérieurs
    secteur_public_prive_libe: string
    effectifs: number
    libelle_commune: string
    etab_prioritaire?: string
  }
export interface DataEducation {
  // Effectifs France
  effectifs_premier_et_second_france: number
  effectifs_premier_et_second_prive_france: number
  effectifs_primaire_france: number
  effectifs_primaire_prive_france: number
  effectifs_college_france: number
  effectifs_college_prive_france: number
  effectifs_lycee_france: number
  effectifs_lycee_prive_france: number
  effectifs_superieur_france: number
  effectifs_superieur_prive_france: number
  // Effectifs public + privé circo
  total_effectifs_primaire: number
  total_effectifs_college: number
  total_effectifs_lycee: number
  total_effectifs_superieur: number
  total_effectifs_autre: number
  total_effectifs_primaire_et_secondaire: number
  // Effectifs privé
  total_effectifs_primaire_prive: number
  total_effectifs_college_prive: number
  total_effectifs_lycee_prive: number
  total_effectifs_superieur_prive: number
  total_effectifs_autre_prive: number
  total_effectifs_primaire_et_secondaire_prive: number
  // IPS
  ips_premier_et_second_circo_list: number[]
  ips_premier_et_second_public_circo_list: number[]
  ips_premier_et_second_prive_circo_list: number[]
  ips_premier_et_second_public_france: number
  ips_premier_et_second_prive_france: number
  ips_premier_et_second_france: number
  ips_premier_et_second_public_circo: number
  ips_premier_et_second_prive_circo: number
  ips_premier_et_second_circo_avg: number
  ips_premier_et_second_france_max: number
  ips_premier_et_second_france_min: number
  ips_premier_et_second_prive_france_max: number
  ips_premier_et_second_prive_france_min: number
  ips_premier_et_second_public_france_max: number
  ips_premier_et_second_public_france_min: number
  ips_premier_et_second_circo_max: number
  ips_premier_et_second_circo_min: number
  ips_premier_et_second_prive_circo_max: number
  ips_premier_et_second_prive_circo_min: number
  ips_premier_et_second_public_circo_max: number
  ips_premier_et_second_public_circo_min: number

  nombre_etablissement: number
  total_effectifs: number
  ips_mean: number
  etablissements_premier_et_second_degre: EducationInfoPS[]
  liste_primaires: EducationInfoPS[]
  liste_colleges: EducationInfoPS[]
  liste_lycees: EducationInfoPS[]
  liste_secondaires: EducationInfoPS[]
  liste_autres: EducationInfoPS[]
  etablissements_superieur: EducationInfoSup[]
  etablissements_tous: EducationInfoPS[]
}

  export function build_data_education(dataEducation: DataEducation) {
    // On converti les données pour les établissements supérieurs au format des établissements primaires et secondaires, pour avoir une seule liste
    let etablissements_superieur_tmp=[]
    if(dataEducation.etablissements_superieur){
        etablissements_superieur_tmp = dataEducation.etablissements_superieur.map(
        ({
          latitude: latitude,
          longitude: longitude,
          effectifs : effectifs,
          secteur_public_prive_libe: secteur_public_prive_libe,
          type_d_etablissement: nature_uai_libe,
          libelle: appellation_officielle,
          libelle_commune: libelle_commune,
        }) => ({
          latitude,
          longitude,
          effectifs,
          secteur_public_prive_libe,
          nature_uai_libe,
          appellation_officielle,
          libelle_commune,
        })
      )
    }
    dataEducation.etablissements_tous = dataEducation.etablissements_premier_et_second_degre.concat( etablissements_superieur_tmp || [])
    // École primaire  = maternelle et élémentaire
    dataEducation.liste_primaires =
      dataEducation.etablissements_premier_et_second_degre.filter(
        (d) =>
          d.nature_uai_libe === "ECOLE DE NIVEAU ELEMENTAIRE" ||
          d.nature_uai_libe === "ECOLE MATERNELLE",
      )
    dataEducation.liste_colleges =
      dataEducation.etablissements_premier_et_second_degre.filter((d) =>
        d.nature_uai_libe.startsWith("COLLEGE"),
      )
    dataEducation.liste_lycees =
      dataEducation.etablissements_premier_et_second_degre.filter((d) =>
        d.nature_uai_libe.startsWith("LYCEE"),
      )
    dataEducation.liste_secondaires = dataEducation.liste_colleges.concat(dataEducation.liste_lycees)
    // Other schools
    dataEducation.liste_autres =
      dataEducation.etablissements_premier_et_second_degre.filter(
        (d) =>
          !(
            d.nature_uai_libe === "ECOLE DE NIVEAU ELEMENTAIRE" ||
            d.nature_uai_libe === "ECOLE MATERNELLE" ||
            d.nature_uai_libe.startsWith("COLLEGE") ||
            d.nature_uai_libe.startsWith("LYCEE")
          ),
      )
    dataEducation.total_effectifs_autre = dataEducation.liste_autres.reduce(
      (a, b) => Number(a) + Number(b.effectifs),
      0,
    )

    dataEducation.total_effectifs_primaire =
      dataEducation.liste_primaires.reduce(
        (acc, cur) => Number(acc) + Number(cur.effectifs || 0),
        0,
      )
    dataEducation.total_effectifs_college = dataEducation.liste_colleges.reduce(
      (acc, cur) => Number(acc) + Number(cur.effectifs || 0),
      0,
    )
    dataEducation.total_effectifs_lycee = dataEducation.liste_lycees.reduce(
      (acc, cur) => Number(acc) + Number(cur.effectifs || 0),
      0,
    )
    // --------- IPS ---------
    const liste_etab_prim_second_public_ips =
    dataEducation.etablissements_premier_et_second_degre.filter(
      (d) =>
        d.secteur_public_prive_libe === "Public" && Number(d.ips || 0) > 1,
    )
    const liste_etab_prim_second_prive_ips =
    dataEducation.etablissements_premier_et_second_degre.filter(
      (d) =>
        d.secteur_public_prive_libe != "Public" && Number(d.ips || 0) > 1,
    )
    dataEducation.ips_premier_et_second_prive_circo_list = liste_etab_prim_second_prive_ips.map((d) => d.ips)
    dataEducation.ips_premier_et_second_circo_list = dataEducation.etablissements_premier_et_second_degre
    .filter((d) => Number(d.ips || 0) > 1)
    .map((d) => Number(d.ips),)
    dataEducation.ips_premier_et_second_circo_max = Math.max(...dataEducation.ips_premier_et_second_circo_list)
    dataEducation.ips_premier_et_second_circo_min = Math.min(...dataEducation.ips_premier_et_second_circo_list)
    
    dataEducation.ips_premier_et_second_prive_circo_max = Math.max(...liste_etab_prim_second_prive_ips.map((d) => Number(d.ips)))
    dataEducation.ips_premier_et_second_prive_circo_min = Math.min(...liste_etab_prim_second_prive_ips.map((d) => Number(d.ips)))
    dataEducation.ips_premier_et_second_public_circo_max = Math.max(...liste_etab_prim_second_public_ips.map((d) => Number(d.ips)))
    dataEducation.ips_premier_et_second_public_circo_min = Math.min(...liste_etab_prim_second_public_ips.map((d) => Number(d.ips)))

    dataEducation.ips_premier_et_second_public_circo_list = liste_etab_prim_second_public_ips.map((d) => d.ips)
    dataEducation.ips_premier_et_second_public_circo =
      liste_etab_prim_second_public_ips.reduce(
        (acc, cur) => acc + Number(cur.ips),
        0,
      ) / liste_etab_prim_second_public_ips.length

    dataEducation.ips_premier_et_second_prive_circo =
      liste_etab_prim_second_prive_ips.reduce(
        (acc, cur) => acc + Number(cur.ips),
        0,
      ) / (liste_etab_prim_second_prive_ips.length || 1)
    // Compute the mean of IPS score of all schools
    let nb_etab_with_ips = 0
    const ips_sum = dataEducation.etablissements_premier_et_second_degre.reduce(
      (a, b) => {
        if (Number(b.ips)) {
          nb_etab_with_ips++
          return Number(a) + Number(b.ips)
        } else {
          return Number(a)
        }
      },
      0,
    )
    dataEducation.ips_premier_et_second_circo_avg = ips_sum / nb_etab_with_ips

    if (dataEducation.etablissements_superieur) {
      dataEducation.total_effectifs_superieur =
        dataEducation.etablissements_superieur.reduce(
          (acc, cur) => Number(acc) + Number(cur.effectifs || 0),
          0,
        )
    } else {
      dataEducation.total_effectifs_superieur = 0
      // TODO: Trouver l'indicateur Public/privé pour les effectifs des établissements supérieurs
      dataEducation.total_effectifs_superieur_prive = 0
    }
    dataEducation.nombre_etablissement =
      dataEducation.etablissements_premier_et_second_degre.length +
      (dataEducation.etablissements_superieur?.length || 0)
    dataEducation.total_effectifs =
      dataEducation.total_effectifs_primaire +
      dataEducation.total_effectifs_college +
      dataEducation.total_effectifs_lycee +
      dataEducation.total_effectifs_superieur +
      dataEducation.total_effectifs_autre
    dataEducation.total_effectifs_primaire_et_secondaire =
      dataEducation.total_effectifs_primaire +
      dataEducation.total_effectifs_college +
      dataEducation.total_effectifs_lycee
    // Compute the effectif of private schools
    dataEducation.total_effectifs_primaire_prive =
      dataEducation.liste_primaires.reduce((a, b) => {
        if (b.secteur_public_prive_libe != "Public") {
          return Number(a) + Number(b.effectifs || 0)
        } else {
          return Number(a)
        }
      }, 0)
    dataEducation.total_effectifs_college_prive =
      dataEducation.liste_colleges.reduce((a, b) => {
        if (b.secteur_public_prive_libe != "Public") {
          return Number(a) + Number(b.effectifs || 0)
        } else {
          return Number(a)
        }
      }, 0)
    dataEducation.total_effectifs_lycee_prive =
      dataEducation.liste_lycees.reduce((a, b) => {
        if (b.secteur_public_prive_libe != "Public") {
          return Number(a) + Number(b.effectifs || 0)
        } else {
          return Number(a)
        }
      }, 0)

    dataEducation.total_effectifs_autre_prive =
      dataEducation.liste_autres.reduce((a, b) => {
        if (b.secteur_public_prive_libe != "Public") {
          return Number(a) + Number(b.effectifs || 0)
        } else {
          return Number(a)
        }
      }, 0)
    dataEducation.total_effectifs_primaire_et_secondaire_prive =
      dataEducation.total_effectifs_primaire_prive +
      dataEducation.total_effectifs_college_prive +
      dataEducation.total_effectifs_lycee_prive
  }