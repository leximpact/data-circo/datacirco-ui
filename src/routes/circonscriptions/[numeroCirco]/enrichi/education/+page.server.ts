import type { PageServerLoad } from "./$types"
import config from "$lib/server/config"
export const load = (async ({ params }) => {
  const url = `https://leximpact.an.fr/datacirco/data-json/${params.numeroCirco}/`

  async function getFile(filename: string) {
    const response = await fetch(url + filename)
    if (!response.ok) {
      return null
    }
    return await response.json()
  }
  async function getURLContent(filename: string) {
    const response = await fetch(filename)
    if (!response.ok) {
      return null
    }
    return await response.json()
  }
  const commonData = getFile("common_data.json")
  const dataPopulation = getFile("population.json")
  const dataEducation = getFile("education.json")
  const sources = await (
    await fetch("https://leximpact.an.fr/datacirco/data-json/sources.json")
  ).json()

  return {
    commonData,
    dataPopulation,
    dataEducation,
    sources,
    config,
  }
}) satisfies PageServerLoad
