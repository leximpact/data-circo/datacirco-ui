import type { PageServerLoad } from "./$types"
export const load = (async ({ params }) => {
  const url = `https://leximpact.an.fr/datacirco/data-json/${params.numeroCirco}/`

  async function getFile(filename: string) {
    const response = await fetch(url + filename)
    console.log("downloding", url + filename)
    if (!response.ok) {
      return null
    }
    return await response.json()
  }
  async function getURLContent(url: string) {
    const response = await fetch(url)
    if (!response.ok) {
      return null
    }
    return await response.json()
  }

  const sources = getURLContent("https://leximpact.an.fr/datacirco/data-json/sources.json")
  const commonData = getFile("common_data.json")
  const dataElections = getFile("elections.json")
  const dataEntreprises = getFile("entreprises.json")
  const dataPopulation = getFile("population.json")
  const dataEmploi = getFile("emploi.json")
  const dataLogement = getFile("logement.json")
  const dataSante = getFile("sante.json")
  const dataClimat = getFile("meteo.json")
  const dataEducation = getFile("education.json")
  // const dataEducation = getURLContent("http://localhost:5173/education.json")
  
  return {
    commonData,
    dataElections,
    dataEntreprises,
    dataPopulation,
    dataEmploi,
    dataLogement,
    dataSante,
    dataClimat,
    dataEducation,
    sources,
  }
}) satisfies PageServerLoad
