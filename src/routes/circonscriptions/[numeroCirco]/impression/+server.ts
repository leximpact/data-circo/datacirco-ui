import { chromium } from "playwright"

import type { RequestHandler } from "./$types"

export const GET: RequestHandler = async ({ params, url }) => {
  /*const dirPath = `https://leximpact.an.fr/datacirco/data-json/${params.numeroCirco}`
  const response = await fetch(dirPath)
  if (response.status === 404) {
    throw error(404, "Circonscription inconnue")
  }*/

  const browser = await chromium.launch()
  const page = await browser.newPage()
  await page.goto(
    new URL(
      `/circonscriptions/${params.numeroCirco}/impression`,
      url.origin,
    ).toString(),
  )

  // the locator #precipitations is visible in every circo except 075-05, which is where the locator #dpe is visible
  // TODO: Pourquoi ???
  const locatorFound = page
    .locator("#precipitations")
    .or(page.locator("#dpe"))
    .nth(0)
  await locatorFound.waitFor({ state: "visible" })

  const pdfBuffer = await page.pdf({
    printBackground: true,
    format: "A4",
    displayHeaderFooter: true,
    headerTemplate: "<div></div>",
    footerTemplate:
      '<div style="margin-right: 36px; margin-bottom: 36px; font-size: 10px; text-align: right; width: 100%;"><span class="pageNumber"></span></div>',
  })
  await browser.close()
  return new Response(pdfBuffer, {
    headers: { "Content-Type": "application/pdf" },
  })
}
