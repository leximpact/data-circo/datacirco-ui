echo "Starting script at `date`"

rm -r pdf/**
mkdir -p pdf
mkdir -p reduced-pdf
rm reduced-pdf/*
cd pdf

for c in $(cat ../circo.txt)
do
    wget -O $c.pdf "https://datacirco-web-integ.leximpact.dev/circonscriptions/$c/impression"
    echo "$c Création de la vignette"
    convert $c.pdf[0] -resize 50% -background white -flatten -type palette $c.pdf.png > /dev/null
    optipng $c.pdf.png > /dev/null 2>/dev/null
    mv $c.pdf.png ../reduced-pdf/
    gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile=../reduced-pdf/$c.pdf $c.pdf
done
echo "Done script `date` in $SECONDS s at `date`"